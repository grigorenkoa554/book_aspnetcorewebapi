using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Book_AspNetCoreWebApi.Model;
using Book_AspNetCoreWebApi.Repository;
using Book_AspNetCoreWebApi.Servces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;

namespace Book_AspNetCoreWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IRecalculateFullSalaryService, RecalculateFullSalaryService>();
            services.AddScoped<IBaseRepository<EmployeeRate>, BaseRepository<EmployeeRate>>();
            services.AddScoped<IBaseRepository<Salary>, BaseRepository<Salary>>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IBaseRepository<User>, BaseRepository<User>>();
            services.AddScoped<IBaseRepository<File>, BaseRepository<File>>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IBaseRepository<Planet>, BaseRepository<Planet>>();
            services.AddScoped<IPlanetService, PlanetService>();

            services.AddScoped<ICatRepository, CatRepository>();
            services.AddScoped<IBaseRepository<Kitten>, BaseRepository<Kitten>>();
            services.AddScoped<IBaseRepository<Feed>, BaseRepository<Feed>>();
            services.AddScoped<ICatService, CatService>();
            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            );

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
