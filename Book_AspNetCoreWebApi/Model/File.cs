﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspNetCoreWebApi.Model
{
    [Table("File")]
    public class File : Base
    {
        public string FileName { get; set; }
        public string FilePass { get; set; }
    }
}
