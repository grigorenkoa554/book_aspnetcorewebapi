﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspNetCoreWebApi.Model
{
    public class Base
    {
        public int Id { get; set; }
    }
    [Table("User")]
    public class User : Base
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public int TimeOfWork { get; set; }

        public List<Address> Addresses { get; set; }
        public Salary Salary { get; set; }
    }

    [Table("Salary")]
    public class Salary : Base
    {
        public int BaseAmount { get; set; }
        public int FullAmount { get; set; }

        public User User { get; set; }
        public int UserId { get; set; }
    }

    [Table("Address")]
    public class Address : Base 
    {
        public string City { get; set; }
        public string Street { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }

    [Table("EmployeeRate")]
    public class EmployeeRate : Base
    {
        public int Age { get; set; }
        public int Rate { get; set; }
    }
}
