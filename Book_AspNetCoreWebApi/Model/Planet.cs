﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspNetCoreWebApi.Model
{
    [Table("Planet")]
    public class Planet : Base
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Weight_10_24 { get; set; }
        public int ТumberOfSatellites { get; set; }
    }
}
