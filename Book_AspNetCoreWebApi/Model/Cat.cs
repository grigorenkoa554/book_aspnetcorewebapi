﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspNetCoreWebApi.Model
{
    [Table("Cat")]
    public class Cat : Base
    {
        public string Name { get; set; }

        public List<Kitten> Kittens { get; set; }
        public Feed Feed { get; set; }
    }

    [Table("Kitten")]
    public class Kitten : Base
    {
        public string Color { get; set; }
        public int Age { get; set; }

        public int CatId { get; set; }
        public Cat Cat { get; set; }

    }

    [Table("Feed")]
    public class Feed : Base
    {
        public string Designation { get; set; }
        public string ExpirationDate { get; set; }

        public Cat Cat { get; set; }
        public int CatId { get; set; }
    }
}
