﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Book_AspNetCoreWebApi.Migrations
{
    public partial class AddTimeOfWork : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TimeOfWork",
                table: "User",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TimeOfWork",
                table: "User");
        }
    }
}
