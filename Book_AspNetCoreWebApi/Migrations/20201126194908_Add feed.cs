﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Book_AspNetCoreWebApi.Migrations
{
    public partial class Addfeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Kittens_Cats_CatId",
                table: "Kittens");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Kittens",
                table: "Kittens");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Cats",
                table: "Cats");

            migrationBuilder.RenameTable(
                name: "Kittens",
                newName: "Kitten");

            migrationBuilder.RenameTable(
                name: "Cats",
                newName: "Cat");

            migrationBuilder.RenameIndex(
                name: "IX_Kittens_CatId",
                table: "Kitten",
                newName: "IX_Kitten_CatId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Kitten",
                table: "Kitten",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Cat",
                table: "Cat",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Feed",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Designation = table.Column<string>(nullable: true),
                    ExpirationDate = table.Column<DateTime>(nullable: false),
                    CatId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feed", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Feed_Cat_CatId",
                        column: x => x.CatId,
                        principalTable: "Cat",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Feed_CatId",
                table: "Feed",
                column: "CatId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Kitten_Cat_CatId",
                table: "Kitten",
                column: "CatId",
                principalTable: "Cat",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Kitten_Cat_CatId",
                table: "Kitten");

            migrationBuilder.DropTable(
                name: "Feed");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Kitten",
                table: "Kitten");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Cat",
                table: "Cat");

            migrationBuilder.RenameTable(
                name: "Kitten",
                newName: "Kittens");

            migrationBuilder.RenameTable(
                name: "Cat",
                newName: "Cats");

            migrationBuilder.RenameIndex(
                name: "IX_Kitten_CatId",
                table: "Kittens",
                newName: "IX_Kittens_CatId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Kittens",
                table: "Kittens",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Cats",
                table: "Cats",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Kittens_Cats_CatId",
                table: "Kittens",
                column: "CatId",
                principalTable: "Cats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
