﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Book_AspNetCoreWebApi.Model;
using Book_AspNetCoreWebApi.Repository;
using Book_AspNetCoreWebApi.Servces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Book_AspNetCoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatController : ControllerBase
    {
        private readonly ICatRepository catRepository;
        private readonly ICatService catService;
        public CatController(ICatRepository catRepository, ICatService catService)
        {
            this.catRepository = catRepository;
            this.catService = catService;
        }

        [HttpPost("[action]")]
        public Cat AddCat(Cat cat)
        {
            using (catRepository)
            {
                catRepository.Add(cat);
            }
            return cat;
        }

        [HttpGet("[action]")]
        public Cat GetById(int id)
        {
            using (catRepository)
            {
                return catRepository.GetById(id);
            }
        }
        [HttpGet("[action]")]
        public IEnumerable<Cat> GetAll()
        {
            using (catRepository)
            {
                return catRepository.Join(CatJoinType.Kitten | CatJoinType.Feed).GetAll();
            }
        }
        [HttpDelete("[action]/{id}")]
        public string DeleteById(int id)
        {
            using (catRepository)
            {
                catRepository.Delete(id);
                return $"We are delete Cat with id = {id}";
            }
        }
        [HttpGet("[action]/{id}")]
        public string TheMostLittle(int id)
        {
            return catService.TheMostLittleKittenKitten(id);
        }

        [HttpGet("[action]/{str}")]
        public string CountKittensWithThisFeedOfCat(string str)
        {
            return catService.CountKittensWithThisFeedOfCat(str);
        }

        // GET: api/<CatController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<CatController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<CatController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<CatController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<CatController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
