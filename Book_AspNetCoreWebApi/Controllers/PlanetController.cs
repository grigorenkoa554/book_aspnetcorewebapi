﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Book_AspNetCoreWebApi.Model;
using Book_AspNetCoreWebApi.Repository;
using Book_AspNetCoreWebApi.Servces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Book_AspNetCoreWebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PlanetController : ControllerBase
    {
        private readonly IBaseRepository<Planet> basePlanetRepository;
        private readonly IPlanetService planetService;
        public PlanetController(IBaseRepository<Planet> baseFileRepository, IPlanetService planetService)
        {
            this.basePlanetRepository = baseFileRepository;
            this.planetService = planetService;
        }

        [HttpPost]
        public string AddPlanet(Planet planet)
        {
            using (basePlanetRepository)
            {
                basePlanetRepository.Add(planet);
                return $"Add: {planet.Id}";
            }
        }

        [HttpGet]
        public Planet GetById(int id)
        {
            using (basePlanetRepository)
            {
                return basePlanetRepository.GetById(id);
            }
        }

        [HttpGet]
        public IEnumerable<Planet> GetAll()
        {
            using (basePlanetRepository)
            {
                return basePlanetRepository.GetAll();
            }
        }

        [HttpDelete]
        public string DeleteById(int id)
        {
            using (basePlanetRepository)
            {
                basePlanetRepository.Delete(id);
                return $"Deleted planets Id = {id}";
            }
        }

        [HttpGet]
        public IEnumerable<Planet> PlanetWithWeightMore50kg_10_24()
        {
            var list = planetService.ListPlanetWithWeightMore50kg_10_24();
            return list;
        }

        [HttpDelete]
        public void DeleteWithWeightLess1kg_10_24()
        {
            planetService.DeletePlanetWithWeightLess1kg_10_24();
        }

        // GET: api/<PlanetController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<PlanetController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<PlanetController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<PlanetController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<PlanetController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

