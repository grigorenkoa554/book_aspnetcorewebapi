﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Book_AspNetCoreWebApi.DbContextForWebApi;
using Book_AspNetCoreWebApi.Model;
using Book_AspNetCoreWebApi.Repository;
using Book_AspNetCoreWebApi.Servces;
using Microsoft.AspNetCore.Mvc;

namespace Book_AspNetCoreWebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [Route("api2/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IBaseRepository<User> baseUserRepository;
        private readonly IUserService userService;

        public UserController(IBaseRepository<User> baseUserRepository, IUserService userService)
        {
            this.baseUserRepository = baseUserRepository;
            this.userService = userService;
        }

        [HttpGet("/some1/{str}")]
        public string FirstGet(string str, string str2)
        {
            return $"Some GET response: {str}, {str2}";
        }
        [HttpPost("/some1/{str}")]
        public string FirstPost(string str, string str2)
        {
            return $"Some POST response2: {str}, {str2}";
        }
        [HttpPost("/some2")]
        public string SecondPost(User user)
        {
            using (baseUserRepository)
            {
                baseUserRepository.Add(user);
            }
            return $"User was added. FirstName: {user.FirstName}, Id: {user.Id}";
        }
        [HttpPost("/some3")]
        public string SecondPost3(User[] users)
        {
            using (baseUserRepository)
            {
                baseUserRepository.AddRange(users);
            }
            return $"Ok added.";
        }

        [HttpPost("/post4")]
        public IEnumerable<User> SecondPost4(User[] users)
        {
            using (baseUserRepository)
            {
                baseUserRepository.AddRange(users);
            }
            return users;
        }



        [HttpGet]
        [HttpGet("ComeOn")]
        public User Show()
        {
            var user = new User
            {
                FirstName = "Misha",
                LastName = "Popov",
                Addresses = new List<Address> { new Address { Street = "Sovetskaya", City = "Homel" } }
            };
            return user;
        }

        [HttpPost]
        public string AddUser()
        {
            using (baseUserRepository)
            {
                var listik = new List<User>
                {
                    new User { FirstName = "Lola", LastName = "Narechenkova", Age = 56 },
                    new User { FirstName = "Oleg", LastName = "Hudognikovich", Age = 87 },
                    new User {FirstName = "Karina", LastName = "Hudognikov", Age = 35 }              //4
                };
                baseUserRepository.AddRange(listik);
            }

            //var context = new MyDbContext();
            //context.Users.Add(new User { FirstName = "Lola", LastName = "Narechenkova", Age = 56 });
            //context.Users.Add(new User { FirstName = "Oleg", LastName = "Hudognikovich", Age = 87 });
            //context.Users.Add(new User { FirstName = "Karina", LastName = "Hudognikov", Age = 35 });
            //context.Users.Add(new User { FirstName = "Dima", LastName = "Hudognikovichev" });
            //context.SaveChanges();
            //Console.WriteLine(context);
            return "Ok:)";
        }

        [HttpPost]
        public string AddSalary()
        {
            return "Ok";
            using (var userRep = new BaseRepository<User>())
            using (var salaryRep = new BaseRepository<Salary>())
            {
                var users = userRep.GetAll();
                foreach (var item in users)
                {
                    salaryRep.Add(new Salary
                    {
                        BaseAmount = 3000,
                        FullAmount = 3000,
                        UserId = item.Id
                    });
                }
            }
            return "Ok:)";
        }

        [HttpGet]
        public IEnumerable<User> GetAll()
        {
            using (baseUserRepository)
            {
                return baseUserRepository.GetAll();
            }
            //var context = new MyDbContext();
            //Console.WriteLine(context);
            //return context.Users.ToList();
        }

        [HttpGet]
        public User GetById(int id)
        {
            using (baseUserRepository)
            {
                return baseUserRepository.GetById(id);
            }
            //var repository = new BaseRepository<User>();
            //var user = repository.GetById(id);
            //return user;
        }

        [HttpDelete]
        public string DeleteById(int id)
        {
            using (baseUserRepository)
            {
                baseUserRepository.Delete(id);
                return $"All fine. We are delete next User with id: Id = {id}";
            }
            //var repos = new BaseRepository<User>();
            //var user = repos.GetById(id);
            //string str = $"We are delete next User: Id = {user.Id}, LastName = {user.LastName}, FirstName = {user.FirstName}";
            //repos.Delete(user.Id);
            //return str;
        }

        [HttpDelete]
        public string DeleteByNullAge()
        {
            var result = userService.DeleteByNullAge();
            return $"We are delete {result} elements with age < 1";
        }

        [HttpDelete]
        [Route("/api/[action]/{id}")]
        [ActionName("DelByIdAndShowAll")]

        public IEnumerable<User> DeleteByIdAndShow(int id)
        {
            var repos = new BaseRepository<User>();
            var user = repos.GetById(id);
            repos.Delete(user.Id);
            return GetAll();
        }

        [HttpGet]
        [Route("{str}")]
        [Route("/ShowFamilias/{str}")]
        public IEnumerable<User> ShowUsersWithSomeLastName(string str)
        {
            var context = new MyDbContext();
            var repos = new BaseRepository<User>();
            var list = context.Users.Where(p => p.LastName.Contains(str)).ToList();
            return list;
        }

        [HttpPut]
        public IEnumerable<User> UpdateAndShow(User user)
        {
            var repos = new BaseRepository<User>();
            repos.Update(user);
            return GetAll();
        }
    }
}

// GET      - GET
// POST     - ADD
// PUT      - UPDATE
// DELETE   - DELETE