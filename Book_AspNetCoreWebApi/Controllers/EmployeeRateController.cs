﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Book_AspNetCoreWebApi.DbContextForWebApi;
using Book_AspNetCoreWebApi.Model;
using Book_AspNetCoreWebApi.Repository;
using Book_AspNetCoreWebApi.Servces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Book_AspNetCoreWebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EmployeeRateController : ControllerBase
    {
        private readonly IRecalculateFullSalaryService recalculateFullSalary;
        private readonly IBaseRepository<EmployeeRate> baseRepository;

        public EmployeeRateController(IRecalculateFullSalaryService recalculateFullSalary,
            IBaseRepository<EmployeeRate> baseRepository)
        {
            this.recalculateFullSalary = recalculateFullSalary;
            this.baseRepository = baseRepository;
        }

        [HttpGet]
        public string RecalculateFullSalary()
        {
            recalculateFullSalary.Recalculate();
            return "Ok...";
        }


        [HttpPost]
        public string AddEmployeeRate()
        {
            var list = new List<EmployeeRate>
            {
                new EmployeeRate { Age = 10, Rate = 5 },
                new EmployeeRate { Age = 20, Rate = 10},
                new EmployeeRate { Age = 30, Rate = 15},
                new EmployeeRate { Age = 40, Rate = 20}
            };
            using (baseRepository)
            {
                baseRepository.AddRange(list);
            }
            //var context = new MyDbContext();
            //context.EmployeeRates.Add(new EmployeeRate { Age = 10, Rate = 5});
            //context.EmployeeRates.Add(new EmployeeRate { Age = 20, Rate = 10});
            //context.EmployeeRates.Add(new EmployeeRate { Age = 30, Rate = 15});
            //context.EmployeeRates.Add(new EmployeeRate { Age = 40, Rate = 20});

            //context.SaveChanges();

            return "Ok...";
        }
        [HttpGet]
        public IEnumerable<EmployeeRate> GetAll()
        {
            using (baseRepository)
            {
                return baseRepository.GetAll();
            }
            //var context = new MyDbContext();
            //Console.WriteLine(context);
            //return context.EmployeeRates;
        }


        [HttpGet]
        [Route("/api/[action]/{id}")]
        public EmployeeRate GetById(int id)
        {
            using (baseRepository)
            {
                return baseRepository.GetById(id);
            }
            //    var repository = new BaseRepository<EmployeeRate>();
            //var employeeRate = repository.GetById(id);
            //return employeeRate;
        }

        // GET: api/<EmployeeRateController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<EmployeeRateController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<EmployeeRateController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<EmployeeRateController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EmployeeRateController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
