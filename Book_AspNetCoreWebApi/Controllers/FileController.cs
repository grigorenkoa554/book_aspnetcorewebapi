﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Book_AspNetCoreWebApi.Model;
using Book_AspNetCoreWebApi.Repository;
using Book_AspNetCoreWebApi.Servces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Book_AspNetCoreWebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IBaseRepository<File> baseFileRepository;
        private readonly IFileService fileService;
        public FileController(IBaseRepository<File> baseFileRepository, IFileService fileService)
        {
            this.baseFileRepository = baseFileRepository;
            this.fileService = fileService;
        }

        [HttpPost]
        public string AddFile(File file)
        {
            using (baseFileRepository)
            {
                baseFileRepository.Add(file);
                return $"Added {file.Id}";
            }
        }

        [HttpPost]
        public string AddFiles()
        {
            using (baseFileRepository)
            {
                baseFileRepository.Add(new Model.File
                {
                    FileName = "InportantWordFile1.docx",
                    FilePass = @"C:\Users\nastj\Desktop\"
                });
                baseFileRepository.Add(new Model.File
                {
                    FileName = "Presentation1.pptx",
                    FilePass = @"C:\Users\"
                });
                baseFileRepository.Add(new Model.File
                {
                    FileName = "InportantWordFile2.docx",
                    FilePass = @"C:\Users\nastj\Desktop\"
                });
                baseFileRepository.Add(new Model.File
                {
                    FileName = "Presentation2.pptx",
                    FilePass = @"C:\Users\"
                });
                baseFileRepository.Add(new Model.File
                {
                    FileName = "Presentation3.pptx",
                    FilePass = @"C:\Users\nastj\Desktop\"
                });

                return $"Added count: {5}.";
            }
        }

        [HttpGet]
        public File GetById(int id)
        {
            using (baseFileRepository)
            {
                return baseFileRepository.GetById(id);
            }
        }

        [HttpGet]
        public IEnumerable<File> GetAll()
        {
            using (baseFileRepository)
            {
                return baseFileRepository.GetAll();
            }
        }

        [HttpDelete]
        public string DeleteById(int id)
        {
            using (baseFileRepository)
            {
                baseFileRepository.Delete(id);
                return $"We are delete next File where Id = {id}";
            }
        }

        [HttpGet("{pass}")]
        public string GetCountOfFiles(string pass)
        {
            var count = fileService.GetCountOfFilesOnPass(pass);
            return $"We have {count} files on pass: {pass}";
        }

        [HttpGet("{pass}")]
        public IEnumerable<File> GetFileWithSamePass(string pass)
        {
            var list = fileService.GetFileWithSamePass(pass);
            return list;
        }

        // GET: api/<FileController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<FileController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<FileController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<FileController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<FileController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
