﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Book_AspNetCoreWebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MySecondController : ControllerBase
    {
        // GET: api/<MySecondController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<MySecondController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value" + id;
        }

        [HttpGet("{id}")]
        public string Get2(int id)
        {
            return "value" + id;
        }

        [HttpGet("second/{id}")]
        //[ArrayInput("id", Separator = ';')]
        public string Get22224(int id, [FromQuery] string[] args)
        {
            var result = "";
            foreach (var item in args)
            {
                result += item;
            }
            return $"{result} - {id}";
        }

        // POST api/<MySecondController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<MySecondController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<MySecondController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
