﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Book_AspNetCoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MyFirstController : ControllerBase
    {
        // GET: api/<MyFirstController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<MyFirstController>/5
        [HttpGet("{id}")]
        public string Get22222(int id)
        {
            return "value" + id;
        }

        // GET api/<MyFirstController>/5
        [HttpGet("second/{id}")]
        public string Get22224(int id)
        {
            return "value" + id;
        }

        // POST api/<MyFirstController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<MyFirstController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<MyFirstController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
