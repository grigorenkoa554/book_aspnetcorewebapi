﻿using Book_AspNetCoreWebApi.Model;
using Book_AspNetCoreWebApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspNetCoreWebApi.Servces
{
    public interface ICatService
    {
        int ShowCountOfCatKittens(int id);
        string TheMostLittleKittenKitten(int id);
        string CountKittensWithThisFeedOfCat(string nameOfFeed);
    }

    public class CatService : ICatService
    {
        private readonly ICatRepository catRepository;
        public CatService(ICatRepository baseRepository)
        {
            this.catRepository = baseRepository;
        }
        public int ShowCountOfCatKittens(int id)
        {
            using (catRepository)
            {
                var list = new List<Kitten>();
                var cat = catRepository.Join(CatJoinType.Kitten).GetById(id);
                foreach (var item in cat.Kittens)
                {
                    list.Add(item);
                }
                return list.Count();
            }
        }

        public string TheMostLittleKittenKitten(int id)
        {
            using (catRepository)
            {
                var list = new List<Kitten>();
                var cat = catRepository.Join(CatJoinType.Kitten).GetById(id);
                var firstKitten = cat.Kittens.First();
                // var minAge = cat.Kittens.Min(x => x.Age);
                // var minId = cat.Kittens.First(x => x.Age == minAge); 
                var littleAge = firstKitten.Age;
                int litteAgeId = firstKitten.Id;
                foreach (var item in cat.Kittens)
                {
                    if (item.Age < littleAge)
                    {
                        littleAge = item.Age;
                        litteAgeId = item.Id;
                    }
                }
                return $"Kitten Of Car with Id = {id} have the most little kitten with id = {litteAgeId} and age = {littleAge}";

            }
        }

        public string CountKittensWithThisFeedOfCat(string nameOfFeed)
        {
            using (catRepository)
            {
                var listCats = catRepository.Join(CatJoinType.Feed | CatJoinType.Kitten).GetAll(x => x.Feed.Designation == nameOfFeed);
                var countOfKitten = 0;
                foreach (var item in listCats)
                {
                    countOfKitten += item.Kittens.Count();
                }
                return $"The number of kittens whose mothers eat {nameOfFeed} is {countOfKitten}";
            }
        }
    }
}