﻿using Book_AspNetCoreWebApi.Model;
using Book_AspNetCoreWebApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspNetCoreWebApi.Servces
{
    public interface IFileService
    {
        int GetCountOfFilesOnPass(string pass);
        IEnumerable<File> GetFileWithSamePass(string pass);
    }
    public class FileService : IFileService
    {
        private readonly IBaseRepository<File> baseRepository;
        public FileService(IBaseRepository<File> baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public int GetCountOfFilesOnPass(string pass)
        {
            using (baseRepository)
            {
                var countOfFile = baseRepository.GetAll(x => x.FilePass == pass).Count();
                return countOfFile;
            }
        }

        public IEnumerable<File> GetFileWithSamePass(string pass)
        {
            using (baseRepository)
            {
                var list = baseRepository.GetAll(x => x.FilePass == pass);
                return list;
            }
        }
    }
}
