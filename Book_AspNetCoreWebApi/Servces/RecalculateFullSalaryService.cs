﻿using Book_AspNetCoreWebApi.Model;
using Book_AspNetCoreWebApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspNetCoreWebApi.Servces
{
    public interface IRecalculateFullSalaryService
    {
        void Recalculate();
    }

    public class RecalculateFullSalaryService : IRecalculateFullSalaryService
    {
        private readonly IBaseRepository<EmployeeRate> employeeRepository;
        private readonly IBaseRepository<Salary> salaryRepository;
        private readonly IBaseRepository<User> userRepository;

        public RecalculateFullSalaryService(IBaseRepository<EmployeeRate> employeeRepository,
            IBaseRepository<Salary> salaryRepository,
            IBaseRepository<User> userRepository)                                                   //3
        {
            this.employeeRepository = employeeRepository;
            this.salaryRepository = salaryRepository;
            this.userRepository = userRepository;
        }
        public void Recalculate()
        {
            using (employeeRepository)
            using (salaryRepository)
            using (userRepository)
            {
                var rates = employeeRepository.GetAll().OrderBy(x => x.Age).ToList();
                var allUser = userRepository.GetAll().ToList();
                foreach (var user in allUser)
                {
                    // http://localhost:5000/api/EmployeeRate/RecalculateFullSalary
                    var salary = salaryRepository.GetAll()
                        .Single(x => x.UserId == user.Id);
                    var rate = rates.FirstOrDefault(x => x.Age <= user.TimeOfWork);
                    if (rate != null)
                    {
                        salary.FullAmount = salary.BaseAmount * rate.Rate;
                        salaryRepository.Update(salary);
                    }
                }
            }
        }
    }
}
