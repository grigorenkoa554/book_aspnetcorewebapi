﻿using Book_AspNetCoreWebApi.Model;
using Book_AspNetCoreWebApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspNetCoreWebApi.Servces
{
    public interface IPlanetService
    {
        IEnumerable<Planet> ListPlanetWithWeightMore50kg_10_24();
        string DeletePlanetWithWeightLess1kg_10_24();
    }
    public class PlanetService : IPlanetService
    {
        private readonly IBaseRepository<Planet> baseRepository;
        public PlanetService(IBaseRepository<Planet> baseRepository)
        {
            this.baseRepository = baseRepository;
        }
        public string DeletePlanetWithWeightLess1kg_10_24()
        {
            using (baseRepository)
            {
                var listForDelete = baseRepository.GetAll(x => x.Weight_10_24 < 1);
                foreach (var item in listForDelete)
                {
                    baseRepository.Delete(item.Id);
                }
                return $"{listForDelete.Count} planet(s) was deleted";
            }
        }

        public IEnumerable<Planet> ListPlanetWithWeightMore50kg_10_24()
        {
            using (baseRepository)
            {
                return baseRepository.GetAll(x => x.Weight_10_24 >= 50);
            }
        }
    }
}
