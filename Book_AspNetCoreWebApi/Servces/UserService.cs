﻿using Book_AspNetCoreWebApi.Model;
using Book_AspNetCoreWebApi.Repository;

namespace Book_AspNetCoreWebApi.Servces
{
    public interface IUserService
    {
        int DeleteByNullAge();
    }
    public class UserService : IUserService
    {
        private readonly IBaseRepository<User> baseRepository;
        public UserService(IBaseRepository<User> baseRepository)
        {
            this.baseRepository = baseRepository;
        }
        public int DeleteByNullAge()
        {
            using (baseRepository)
            {
                var list = baseRepository.GetAll(x => x.Age == 0);
                foreach (var item in list)
                {
                    baseRepository.Delete(item.Id);
                }
                return list.Count;
            }
        }
    }
}
