﻿using Book_AspNetCoreWebApi.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Book_AspNetCoreWebApi.Repository
{
    public interface ICatRepository: IBaseRepository<Cat>, IDisposable
    {
        ICatRepository Join(CatJoinType catJoinType);
    }

    public class CatRepository : BaseRepository<Cat>, ICatRepository
    {
        private CatJoinType catJoinType;
        public ICatRepository Join(CatJoinType catJoinType)
        {
            this.catJoinType = catJoinType;
            return this;
        }

        protected override IQueryable<Cat> IncludeJoin()
        {
            var query = base.IncludeJoin();
            if (catJoinType.HasFlag(CatJoinType.Kitten))
            {
                query = query.Include(x => x.Kittens);
            }
            if (catJoinType.HasFlag(CatJoinType.Feed))
            {
                query = query.Include(x => x.Feed);
            }
            return query;
        }
    }

    [Flags]
    public enum CatJoinType
    {
        None = 0,
        Kitten = 1,
        Feed = 2
    }
}
