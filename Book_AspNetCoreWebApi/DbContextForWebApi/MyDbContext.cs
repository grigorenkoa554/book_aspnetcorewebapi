﻿using Book_AspNetCoreWebApi.Model;
using Microsoft.EntityFrameworkCore;

namespace Book_AspNetCoreWebApi.DbContextForWebApi
{
    public class MyDbContext : DbContext
    {
        public MyDbContext()
        {
            Database.EnsureCreated();
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<EmployeeRate> EmployeeRates { get; set; }
        public DbSet<Salary> Salaries { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Planet> Planets { get; set; }
        public DbSet<Cat> Cats { get; set; }
        public DbSet<Kitten> Kittens { get; set; }
        public DbSet<Feed> Feeds { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        {
            var cs = @"Data Source = 'DESKTOP-IOHQV8L'; 
                Initial Catalog='DBForWebApi';
                Integrated Security=true;";
            optionsBuilder.UseSqlServer(cs);
        }
    }
}
